#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse, os, re, socket, subprocess, time
from ast import literal_eval
from datetime import datetime
from hashlib import sha1
from lib.api import ZorillaAPI
from lib.util import load_config, load_yaml


def compare_hash(a, b):
    """
    Compares the contents of two lists/tuples and returns True if they are equal
    """
    list_a = list(a)
    list_b = list(b)
    m_a = sha1()
    m_b = sha1()
    m_a.update("".join(list_a).encode())
    m_b.update("".join(list_b).encode())
    return m_a.hexdigest() == m_b.hexdigest()


def clean_db(api, update_tag, hostname=socket.getfqdn()):
    """
    Removes cruft from the database via API calls
    """
    for fs in api.find_notequals("filesystems", "update_tag", update_tag):
        if fs["hostname"] == hostname:
            print("delete:\t\t{}/{}".format(fs["zpool"], fs["fsname"]))
            api.delete_filesystem(fs["id"])
    for snap in api.find_notequals("snapshots", "update_tag", update_tag):
        fs = api.find_filesystem({"id": snap["filesystem_id"]})
        if fs is None or fs["hostname"] == hostname:
            api.delete_snapshot(snap["id"])


def compare_metadata(new, old):
    """
    Returns True if both sets of metadata are equal
    """
    new_meta = {}
    for key in old.keys():
        new_meta[key] = new[key]
    return compare_hash(
        [str(new_meta[key]) for key in sorted(new_meta.keys())],
        [str(old[key]) for key in sorted(old.keys())],
    )


def get_filesystems(zpool):
    """
    Returns a list of zfs filesystems for a given zpool
    """
    filesystems = []
    for line in runcmd("/sbin/zfs list -H -o name -r {}".format(zpool)):
        if re.match(r"[A-Za-z0-9_.-]+\/[A-Za-z0-9_.-]+", line):
            filesystems.append(line.rstrip())
    return filesystems


def get_fs_metadata(fs_name, hostname=socket.getfqdn()):
    """
    Returns a dict of filesystem metadata
    """
    split_name = split_fsname(fs_name)
    props = get_zfs_properties(fs_name)
    meta = {
        "hostname": hostname,
        "zpool": split_name[0],
        "fsname": split_name[1],
        "mountpoint": props["mountpoint"],
        "owner_id": get_owner_id(fs_name),
        "group_id": get_group_id(fs_name),
        "quota_bytes": props["quota"],
        "refquota_bytes": props["refquota"],
        "used_bytes": props["used"],
        # "sharenfs": (True if props["sharenfs"] == "on" else False),
        # "sharesmb": (True if props["sharesmb"] == "on" else False),
    }
    return meta


def get_group_id(fs):
    """
    Returns a filesystem's top-level group ownership as a gid
    """
    return stat_file("/{}".format(fs), "%g")


def get_owner_id(fs):
    """
    Returns a filesystem's top-level user ownership as a uid
    """
    return stat_file("/{}".format(fs), "%u")


def get_snapshots(fs):
    """
    Returns a list of zfs snapshots for a given filesystem
    """
    snapshots = []
    for line in runcmd("/sbin/zfs list -H -o name -t snapshot -r {}".format(fs)):
        if re.match(r"[A-Za-z0-9_.-]+\/[A-Za-z0-9_.-]+@(.*)", line):
            snapshots.append(line.rstrip())
    return snapshots


def get_zfs_properties(fs):
    """
    Returns a dict of zfs properties
    """
    properties = {}
    for line in runcmd("/sbin/zfs get -H -o property,value -p all {}".format(fs)):
        m = re.match(r"(.*)\t(.*)", line)
        if m:
            properties[m.group(1)] = m.group(2)
    return properties


def process_filesystem(api, fs, update_tag, fqdn):
    """
    Create or update a filesystem via the API
    """
    fs_meta = get_fs_metadata(fs, hostname=fqdn)
    result = api.find_filesystem(
        {
            "fsname": fs_meta["fsname"],
            "zpool": fs_meta["zpool"],
            "hostname": fs_meta["hostname"],
        }
    )
    fs_id = None if result is None else result["id"]
    # if no record was found, insert one
    if result is None:
        print("create:\t\t{}/{}".format(fs_meta["zpool"], fs_meta["fsname"]))
        tagged_meta = fs_meta.copy()
        tagged_meta.update({"update_tag": update_tag})
        return api.create_filesystem(tagged_meta)
    else:
        # else check to see if the content has changed and if so update the row
        if compare_metadata(result, fs_meta) is False:
            print("modify:\t\t{}/{}".format(fs_meta["zpool"], fs_meta["fsname"]))
        else:
            print("no change:\t{}/{}".format(fs_meta["zpool"], fs_meta["fsname"]))
        update_meta = fs_meta.copy()
        update_meta.update({"id": fs_id, "update_tag": update_tag})
        return api.update_filesystem(update_meta)


def process_snapshot(api, snap_name, fs_id, owner_id, update_tag):
    """
    Create or update a snapshot via the API
    """
    result = api.find_snapshot({"name": snap_name})
    snap_meta = {
        "name": snap_name,
        "filesystem_id": fs_id,
        "owner_id": owner_id,
        "update_tag": update_tag,
    }
    if result is None:
        snap_id = api.create_snapshot(snap_meta)["id"]
    else:
        snap_meta.update({"id": result["id"]})
        snap_id = api.update_snapshot(snap_meta)["id"]
    return snap_id


def runcmd(command):
    """
    Runs a system command and returns the lines of output as a list
    """
    cmd = command.split(" ")
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output, errors = p.communicate(timeout=30)
    return output.decode("utf-8").splitlines()


def split_fsname(fs):
    """
    Splits a filesystem string into zpool and filesystem name
    """
    return fs.split("/", 1)


def stat_file(path, format=None):
    """
    Runs 'stat' on a file and returns a formatted string
    """
    result = runcmd("/bin/stat -c '{}' {}".format(format, path))[0]
    m = re.match(r"'(.*)'$", result)
    if m:
        return m.group(1)


def get_backup_config(filename):
    """
    Parse values from backup config file
    """
    conf = load_yaml(filename)
    return {
        "fsname": conf.get("filesystems", {}).get("name", None),
        "zpool": conf.get("filesystems", {}).get("source", {}).get("pool", None),
        "hostname": (conf.get("filesystems", {}).get("source", {}).get("host", None)),
        "retention": (
            conf.get("filesystems", {}).get("source", {}).get("retention", None)
        ),
    }


def update_snapshot_retentions(api, include_dir):
    """
    Update zorilla filesystems snapshot_retention to match backup configs
    """
    for root, dirs, files in os.walk(include_dir):
        for f in files:
            if re.search("\.yml$", f):
                filename = os.path.join(root, f)
                c = get_backup_config(filename)
                fs = api.find_filesystem(
                    {
                        "hostname": c["hostname"],
                        "zpool": c["zpool"],
                        "fsname": c["fsname"],
                    }
                )
                if fs is not None and fs["snapshot_retention"] != c["retention"]:
                    api.update_filesystem(
                        {"id": fs["id"], "snapshot_retention": c["retention"]}
                    )


def main():
    parser = argparse.ArgumentParser(description="Update Zorilla Filesystem Metadata")
    parser.add_argument(
        "--conf", help="Path to conf file", default="/opt/zorro/config.yml"
    )
    args = parser.parse_args()
    conf = load_config(args.conf)
    api = ZorillaAPI(conf["api"]["endpoint"], token=conf["api"]["token"])
    update_tag = time.time()
    if conf["local_filesystems"]:
        for zpool in conf["zpools"]:
            for fs_name in get_filesystems(zpool):
                fs_meta = process_filesystem(api, fs_name, update_tag, conf["fqdn"])
                snapshots = get_snapshots(fs_name)
                for snap_name in snapshots:
                    process_snapshot(
                        api, snap_name, fs_meta["id"], fs_meta["owner_id"], update_tag
                    )
        clean_db(api, update_tag, hostname=conf["fqdn"])
    if conf["local_backup_configs"]:
        update_snapshot_retentions(api, conf["backup"]["include_dir"])


if __name__ == "__main__":
    main()
