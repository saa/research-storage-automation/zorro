#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Listens for commands from zorilla via RabbitMQ
"""

import argparse, json, logging, os, pika, re, stat, socket, subprocess, threading, time, urllib.parse, yaml
from logging import handlers as logging_handlers
from lib.api import ZorillaAPI
from lib.util import load_config


class QueueListener:
    """
    Listen to a RabbitMQ queue and take an action on incoming messages
    """

    def __init__(
        self,
        server="localhost",
        port=5672,
        username="zorilla",
        password="zorilla",
        vhost="/",
        queue="default",
        api=None,
        ssl=True,
        samba_include="/etc/samba/zorilla.conf",
        samba_include_dir="/etc/samba/zorilla.d/",
        backup_include_dir="/etc/zfs-replicate.d/",
        fqdn=socket.getfqdn(),
    ):
        self.server = server
        self.port = port
        self.username = username
        self.password = password
        self.vhost = urllib.parse.quote_plus(vhost)
        self.queue = queue
        self.api = api
        self.ssl = ssl
        self.samba_include = samba_include
        self.samba_include_dir = samba_include_dir
        self.backup_include_dir = backup_include_dir
        self.fqdn = fqdn
        self.connection = self._connect()
        self.channel = self._get_channel()

    def _connect(self):
        """
        Return a RabbitMQ connection
        """
        proto = "amqps" if self.ssl else "amqp"
        url = "{}://{}:{}@{}:{}/{}".format(
            proto, self.username, self.password, self.server, self.port, self.vhost
        )

        logger.debug("Connecting to RabbitMQ: {}".format(url))
        return pika.BlockingConnection(pika.URLParameters(url))

    def _get_channel(self):
        """
        Return a RabbitMQ channel
        """
        return self.connection.channel()

    def onmessage_callback(self, channel, method, properties, body):
        """
        Wraps handle_message to provide ack functionality
        """
        logger.info("New message received from '{}'".format(method.routing_key))
        message = json.loads(body)
        logger.debug("Received: {}".format(message))
        if self.handle_message(message):
            logger.debug("Sending message ack for {}".format(method.delivery_tag))
            channel.basic_ack(delivery_tag=method.delivery_tag)

    def handle_message(self, message):
        """
        Override in subclass to handle incoming messages
        """
        raise NotImplementedError

    def start(self):
        """
        Start consuming messages
        """
        logger.debug("Declaring queue {}".format(self.queue))
        self.channel.queue_declare(queue=self.queue)
        logger.debug("Listening for messages on {}".format(self.queue))
        self.channel.basic_consume(self.onmessage_callback, queue=self.queue)
        self.channel.start_consuming()


class CommonQueueListener(QueueListener):
    """
    Subclass of QueueListener with common helper methods
    """

    def change_group(self, file_path, gid):
        """
        Change a file's group ownership
        """
        logger.info("Changing gid for {} to {}".format(file_path, gid))
        if os.path.ismount(file_path):
            # change the group ownership
            os.chown(file_path, -1, gid)
            # confirm the change
            fileinfo = os.stat(file_path)
            return fileinfo.st_gid == gid
        else:
            return False

    def change_owner(self, file_path, uid):
        """
        Change a file's user ownership
        """
        logger.info("Changing gid for {} to {}".format(file_path, uid))
        if os.path.ismount(file_path):
            # change the group ownership
            os.chown(file_path, uid, -1)
            # confirm the change
            fileinfo = os.stat(file_path)
            return fileinfo.st_uid == uid
        else:
            return False

    def get_filesystem(self, filesystem_id):
        """
        Look up a filesystem by id
        """
        fs = self.api.find_filesystem({"id": filesystem_id})
        logger.debug("Fetching filesystem: {}".format(fs))
        return fs

    def run_command(self, command):
        """
        Generic wrapper for running system commands
        Returns True if execution was successful
        """
        cmd = command.split(" ")
        try:
            output = ""
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            stdout, stderr = p.communicate(timeout=30)
            output = stdout.decode("utf-8").splitlines()
            message = "command: {} output: {}".format(command, " | ".join(output))
            if p.returncode > 0:
                logger.error(message)
            else:
                logger.debug(message)
            return p.returncode <= 0
        except FileNotFoundError:
            return False

    def set_permissions(self, file_path, mode):
        """
        Change a file's user ownership
        """
        logger.info("Changing mode for {} to {}".format(file_path, oct(mode)))
        if os.path.ismount(file_path):
            # change the group ownership
            os.chmod(file_path, mode)
            # confirm the change
            fileinfo = os.stat(file_path)
            return mode == stat.S_IMODE(fileinfo.st_mode)
        else:
            return False


class GroupChangeListener(CommonQueueListener):
    """
    Listen for 'group_change' commands
    """

    def handle_message(self, message):
        """
        Perform group change and update the API
        """
        filesystem = self.get_filesystem(message["filesystem_id"])
        group_id = int(message["group_id"])
        if self.change_group(filesystem["mountpoint"], group_id):
            # update the database via the API
            self.api.update_filesystem({"id": filesystem["id"], "group_id": group_id})
            return True


class BackupProvisionListener(CommonQueueListener):
    """
    Listen for 'provision_backup' commands
    """

    def write_config_file(self, username, config_file, config):
        """
        Create a config for zfs-replicate
        """
        logger.debug("Writing configuration to {}".format(config_file))
        with open(config_file, "w") as file:
            file.write("# provisioned by: {}\n".format(username))
            file.write(yaml.dump(config, default_flow_style=False))

    def handle_message(self, message):
        """
        Create or delete a zfs-replicate config file
        """
        filesystem = self.get_filesystem(message["filesystem_id"])
        username = message["external_uid"]
        filename = "{}/{}.yml".format(self.backup_include_dir, filesystem["fsname"])
        if message["retention_days"] == 0:
            logger.info(
                "Removing backup config for {}/{}".format(
                    filesystem["zpool"], filesystem["fsname"]
                )
            )
            os.remove(filename)
        else:
            logger.info(
                "Generating backup config for {}/{}".format(
                    filesystem["zpool"], filesystem["fsname"]
                )
            )
            config = {
                "filesystems": {
                    "name": filesystem["fsname"],
                    "source": {
                        "host": filesystem["hostname"],
                        "pool": filesystem["zpool"],
                        "retention": message["retention_days"],
                    },
                    "destination": {"retention": message["retention_days"]},
                }
            }
            self.write_config_file(username, filename, config)
            self.api.update_filesystem(
                {
                    "id": filesystem["id"],
                    "snapshot_retention": message["retention_days"],
                }
            )
        return True


class FilesystemProvisionListener(CommonQueueListener):
    """
    Listen for 'provision_filesystem' commands
    """

    def filesystem_exists(self, fsname):
        """
        Check if the filesystem already exists
        """
        try:
            exists = self.run_command("/sbin/zfs list {}".format(fsname))
            logger.debug("Checking if {} exists: {}".format(fsname, exists))
            return exists
        except FileNotFoundError as e:
            logger.debug("Checking if {} exists: False".format(fsname))
            return False

    def create_filesystem(self, fsname):
        """
        Create a new ZFS filesystem
        """
        logger.debug("Creating filesystem {}".format(fsname))
        return self.run_command("/sbin/zfs create {}".format(fsname))

    def set_quota(self, fsname, quota):
        """
        Set ZFS quota property on a filesystem
        """
        logger.debug("Setting {} quota={}".format(fsname, quota))
        return self.run_command("/sbin/zfs set quota={} {}".format(quota, fsname))

    def set_refquota(self, fsname, refquota):
        """
        Set ZFS refquota property on a filesystem
        """
        logger.debug("Setting {} refquota={}".format(fsname, refquota))
        return self.run_command("/sbin/zfs set refquota={} {}".format(refquota, fsname))

    def create_smbshare(self, username, sharename, path, share_params={}):
        """
        Create a samba usershare
        """
        logger.debug("Creating samba usershare {} for {}".format(sharename, username))
        share_file = "{}/{}.conf".format(self.samba_include_dir, username)
        with open(share_file, "w") as file:
            file.write(
                "# provisioned by: {}\n".format(username)
                + "[{}]\n".format(sharename)
                + "\tpath = {}\n".format(path)
                + "\n".join(
                    [
                        "\t{} = {}".format(key, value)
                        for key, value in share_params.items()
                    ]
                )
                + "\n"
            )
        with open(self.samba_include, "a") as file:
            file.write("include = {}\n".format(share_file))
        return self.run_command("/bin/smbcontrol smbd reload-config")

    def handle_message(self, message):
        """
        Create a new ZFS filesystem, set properties and permissions, and update the API
        """
        logger.info("Creating new filesystem {}".format(message["filesystem_name"]))
        fsname = "{}/{}".format(message["zpool"], message["filesystem_name"])
        path = "/{}".format(fsname)
        if not self.filesystem_exists(fsname):
            if (
                self.create_filesystem(fsname)
                and self.set_quota(fsname, message["quota"])
                and self.set_refquota(fsname, message["refquota"])
                and self.change_owner(path, int(message["owner_id"]))
                and self.change_group(path, int(message["group_id"]))
                and self.set_permissions(
                    # AKA "chmod u+rwx,g+rws"
                    path,
                    stat.S_IRWXU | stat.S_IRWXG | stat.S_ISGID,
                )
            ):
                filesystem_metadata = {
                    "owner_id": message["owner_id"],
                    "group_id": message["group_id"],
                    "fsname": message["filesystem_name"],
                    "zpool": message["zpool"],
                    "mountpoint": "/{}".format(fsname),
                    "hostname": self.fqdn,
                    "quota_bytes": message["quota"],
                    "refquota_bytes": message["refquota"],
                    "sharesmb": message["sharesmb"],
                }
                if message["sharesmb"]:
                    filesystem_metadata.update(
                        {"smb_sharename": message["filesystem_name"]}
                    )
                    self.create_smbshare(
                        message["external_uid"],
                        message["filesystem_name"],
                        path,
                        share_params={"writeable": "yes", "inherit acls": "yes"},
                    )
                logger.debug("Updating the API: {}".format(filesystem_metadata))
                self.api.create_filesystem(filesystem_metadata)
                logger.info(
                    "Filesystem {} processing complete".format(
                        message["filesystem_name"]
                    )
                )
                return True


def queue_worker(classname, conf, queue=None, api=None):
    """
    Generic QueueListener worker
    """
    listener = classname(
        server=conf["rabbitmq"]["server"],
        port=conf["rabbitmq"]["port"],
        username=conf["rabbitmq"]["username"],
        password=conf["rabbitmq"]["password"],
        vhost=conf["rabbitmq"]["vhost"],
        ssl=conf["rabbitmq"]["ssl"],
        queue=queue,
        api=api,
        samba_include=conf["samba"]["include_file"],
        samba_include_dir=conf["samba"]["include_dir"],
        backup_include_dir=conf["backup"]["include_dir"],
        fqdn=conf["fqdn"],
    )
    listener.start()


def setup_logger(logfile=None, level="INFO"):
    """
    configures logging handlers
    """
    formatter = logging.Formatter("%(asctime)s %(levelname)s\t%(message)s")
    if logfile is not None:
        log_handler = logging.FileHandler(logfile)
    else:
        log_handler = logging.StreamHandler()
    log_handler.setFormatter(formatter)
    logger.addHandler(log_handler)
    # set logging to level defined log_level
    logger.setLevel(eval("logging.{}".format(level)))
    # set log level for libraries
    logging.getLogger("urllib3").setLevel(logging.ERROR)
    logging.getLogger("pika").setLevel(logging.ERROR)


def main():
    parser = argparse.ArgumentParser(description="Process Commands from Zorilla")
    parser.add_argument(
        "--conf", help="Path to conf file", default="/opt/zorro/config.yml"
    )
    args = parser.parse_args()
    conf = load_config(args.conf)
    setup_logger(level=conf["log_level"])
    logger.info("Starting up...")
    api = ZorillaAPI(conf["api"]["endpoint"], token=conf["api"]["token"])
    threads = []
    logger.info("Spawning worker threads...")
    for t in range(0, 2):
        if "group_change" in conf["rabbitmq"]["queues"]:
            # spawn a GroupChangeListener thread
            thread = threading.Thread(
                target=queue_worker,
                args=(GroupChangeListener, conf, "group_change", api),
                name="thread_{}".format(t),
            )
            thread.daemon = True
            thread.start()
            threads.append(thread)
        if "provision_filesystem" in conf["rabbitmq"]["queues"]:
            # spawn a FilesystemProvisionListener thread
            thread = threading.Thread(
                target=queue_worker,
                args=(FilesystemProvisionListener, conf, "provision_filesystem", api),
                name="thread_{}".format(t),
            )
            thread.daemon = True
            thread.start()
            threads.append(thread)
        if "provision_backup" in conf["rabbitmq"]["queues"]:
            # spawn a BackupProvisionListener thread
            thread = threading.Thread(
                target=queue_worker,
                args=(BackupProvisionListener, conf, "provision_backup", api),
                name="thread_{}".format(t),
            )
            thread.daemon = True
            thread.start()
            threads.append(thread)

    while True:
        pass


logger = logging.getLogger()

if __name__ == "__main__":
    main()
