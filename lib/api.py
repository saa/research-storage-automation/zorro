#!/usr/bin/env python

import logging, requests

try:
    from urllib.parse import quote_plus
except ImportError:
    from urllib import quote_plus


class API:
    """
    Generic DRF API class
    """

    def __init__(self, url, token=None):
        self.url = url
        self.token = token
        self.session = requests.Session()
        self.session.params.update({"format": "json"})

    def _format_filter(self, params, logical_or=False):
        """
        Converts a dict of query parameters into a formatted, urlencoded filter string
        see: https://github.com/philipn/django-rest-framework-filters#complex-querystring-encoding
        """
        encoded = []
        for (key, value) in params.items():
            query_string = quote_plus("{}={}".format(key, value))
            encoded.append("({})".format(query_string))
        if logical_or:
            filter = " | ".join(encoded)
        else:
            filter = " & ".join(encoded)
        if len(filter):
            return "filters={}".format(quote_plus(filter))

    def _request(self, method, url, data={}, headers={}):
        """
        Sends a request including API token header
        """
        if self.token is not None:
            headers.update({"Authorization": "Token {}".format(self.token)})
        request = requests.Request(method, url, data=data, headers=headers)
        prepped = request.prepare()
        clean_headers = headers
        if "Authorization" in clean_headers:
            clean_headers.update({"Authorization": "<REDACTED>"})
        logging.debug(
            "request: {} {} data={} headers={}".format(method, url, data, clean_headers)
        )
        response = self.session.send(prepped)
        logging.debug("response: {}".format(response.content))
        if response.ok:
            return response
        else:
            response.raise_for_status()

    def get(self, path, params={}):
        r = self._request(
            "GET", "{}/{}?{}".format(self.url, path, self._format_filter(params))
        )
        return r.json()

    def put(self, path, data={}):
        r = self._request("PUT", "{}/{}/".format(self.url, path), data=data)
        return r.json()

    def patch(self, path, data={}):
        r = self._request("PATCH", "{}/{}/".format(self.url, path), data=data)
        return r.json()

    def post(self, path, data={}):
        r = self._request("POST", "{}/{}/".format(self.url, path), data=data)
        return r.json()

    def delete(self, path):
        return self._request("DELETE", "{}/{}/".format(self.url, path))

    def create(self, path, data):
        return self.post(path, data=data)

    def read(self, path, params):
        return self.get(path, params=params)

    def read_one(self, path, params):
        try:
            return self.get(path, params=params).pop()
        except (IndexError, AttributeError):
            return None

    def update(self, path, data):
        return self.patch(path, data=data)

    def find_notequals(self, path, key, value):
        response = self._request(
            "GET", "{}/{}/?{}!={}".format(self.url, path, key, value)
        )
        return response.json()


class ZorillaAPI(API):
    """
    Helper methods for the Zorilla REST API
    """

    def create_filesystem(self, fs_meta):
        return self.create("filesystems", fs_meta)

    def create_snapshot(self, snap_meta):
        return self.create("snapshots", snap_meta)

    def read_filesystem(self, fs_meta):
        return self.read_one("filesystems", params=fs_meta)

    def read_snapshot(self, snap_meta):
        return self.read_one("snapshots", params={"name": snap_meta["name"]})

    def update_filesystem(self, fs_meta):
        return self.update("filesystems/{}/".format(fs_meta["id"]), fs_meta)

    def update_snapshot(self, snap_meta):
        return self.update("snapshots/{}/".format(snap_meta["id"]), snap_meta)

    def delete_filesystem(self, id):
        return self.delete("filesystems/{}/".format(id))

    def delete_snapshot(self, id):
        return self.delete("snapshots/{}/".format(id))

    def find_filesystem(self, fs_meta):
        """
        A wrapper for read_filesystem
        """
        return self.read_filesystem(fs_meta)

    def find_snapshot(self, snap_meta):
        """
        A wrapper for read_snapshot
        """
        return self.read_snapshot(snap_meta)
