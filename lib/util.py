import socket, yaml
from itertools import chain


def load_yaml(filename):
    with open(filename, "r") as file:
        return yaml.load(file, Loader=yaml.Loader)


def load_config(filename):
    """
    Load the config file and backfill missing defaults
    """
    # default config values
    defaults = {
        "api": {"endpoint": "https://zorilla.uvm.edu/api/v1/"},
        "backup": {"include_dir": "/etc/zfs-replicate.d/"},
        "fqdn": socket.getfqdn(),
        "log_level": "INFO",
        "rabbitmq": {
            "username": "zorilla",
            "password": "zorilla",
            "server": "zorilla.uvm.edu",
            "port": 5671,
            "vhost": "/",
            "ssl": True,
            "queues": ["group_change", "provision_backup", "provision_filesystem"],
        },
        "samba": {
            "include_file": "/etc/samba/zorilla.conf",
            "include_dir": "/etc/samba/zorilla.d/",
        },
        "zpools": [],
        "local_filesystems": True,
        "local_backup_configs": True,
    }
    merged = {}
    conf = load_yaml(filename)
    for key, value in chain(conf.items(), defaults.items()):
        # loop over subkeys
        if isinstance(value, dict):
            for subkey in value.keys():
                if key not in merged:
                    merged.update({key: {}})
                merged[key].setdefault(subkey, value[subkey])
        else:
            merged.setdefault(key, value)
    # check for missing required values
    try:
        merged["api"]["token"]
    except KeyError as e:
        logger.error("FATAL: Missing configuration value {}".format(e))
        exit(255)
    return merged
